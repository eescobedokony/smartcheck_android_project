package com.bayteq.smartcheckapi;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.konylabs.android.KonyMain;
import com.konylabs.vm.Function;

public class WrapperFFI extends AppCompatActivity {

    public static Function mCallback = null;

    public static void startSmartCheck (Function pCallback, String side) {

        Log.d("&&&&&TestActivityInit", "start");
        TestActivity.mCallback = pCallback;
        Log.d("&&&&&TestActivityInit", " TestActivity.mCallback : "+ TestActivity.mCallback );
        TestActivity.side = side;
        KonyMain context = KonyMain.getActivityContext();
        Intent intent = new Intent(context, TestActivity.class);
        context.startActivity(intent);


        // TODO con el intento usado, determinar la información en este que se grabó en ese intent
        //Log.i("&&&&&TestActivity1", TestActivity.getBase64().toString());
        Log.d("&&&&&TestActivityInit", String.valueOf(intent.getExtras()));
        Log.d("&&&&&TestActivityInit", "end activity");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("&&&&&TestActivityInit", "Request Code: " + requestCode + " ResultCode: " + resultCode);

        finish();
    }

}
