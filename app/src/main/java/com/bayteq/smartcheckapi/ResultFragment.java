package com.bayteq.smartcheckapi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bayteq.smartcheck.services.ResultData;
import com.github.chrisbanes.photoview.PhotoView;

import java.io.File;

/**
 * Created by MNAUNAY on 02/05/2017.
 */

public class ResultFragment extends Fragment {
    private TextView txtStatus;
    private PhotoView imageCheck;
    private TextView txtSize;
    private static final String TAG = "&&&&&ResultFragment";


    public ResultFragment() {
    }

    public static ResultFragment newInstance(ResultData data) {
        ResultFragment fragment = new ResultFragment();
        Bundle args = new Bundle();
        args.putParcelable("data", data);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "&&&&&In ResultFragment");
        View root = inflater.inflate(R.layout.result_data, null, false);
        txtStatus = root.findViewById(R.id.txt_status);
        imageCheck = root.findViewById(R.id.imageView);
        txtSize = root.findViewById(R.id.txt_size);

        Bundle bundle = getArguments();

        try {
            if (bundle.containsKey("data")) {
                ResultData resultData = bundle.getParcelable("data");
                if (resultData.isSuccessful()) {
                    txtStatus.setText("y");
                    txtSize.setText("");
                    try {
                        File file = new File(resultData.getImage());
                        long length = file.length();

                        Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
                        imageCheck.setImageBitmap(bmp);

                        txtSize.setText(String.format("%dx%d - %s KB", bmp.getWidth(), bmp.getHeight(), (length / 1024)));
                    } catch (Exception ex) {
                        ex.printStackTrace();

                    }
                } else {
                    txtStatus.setText(resultData.getError().getDescription());
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return root;
    }
}
