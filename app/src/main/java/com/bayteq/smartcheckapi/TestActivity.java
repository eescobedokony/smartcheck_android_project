package com.bayteq.smartcheckapi;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
//import android.widget.Toast;

import com.bayteq.smartcheck.CaptureActivity;
import com.bayteq.smartcheck.Intents;
import com.bayteq.smartcheck.services.ResultData;
import com.bayteq.smartcheck.SmartCheckAPI;
import com.konylabs.vm.Function;

import java.io.*;
import java.nio.ByteBuffer;

import android.util.Base64;

public class TestActivity extends AppCompatActivity {
    private static final String TAG = "&&&&&TestActivity";
    private static final int REQUEST_CAMERA = 999;
    public static Function mCallback = null;
    public static String side = "FRONT";
    private static Object base64;
    //public Object base64 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //SmartCheckAPI.init(this,"eaccgchjdacejdhfgdfgegehh");//com.bayteq.smartcheckapi
        //SmartCheckAPI.init(this,"eeaiegjegfafeijfddaihikighe");//com.bancobolivariano2
        SmartCheckAPI.init(this,"eagkdgdbdadkececgbhgeeecccfgd");//com.bancobolivariano
        SmartCheckAPI.debugMode();
        super.onCreate(savedInstanceState);
        Intent intent = new Intent(getApplicationContext(), CaptureActivity.class);
        Log.d(TAG, "Side: " + side);
        if (side == "FRONT") {
            intent.putExtra(Intents.EXTRA_TYPE, Intents.CHECK_FRONT);
        } else {
            intent.putExtra(Intents.EXTRA_TYPE, Intents.CHECK_BACK);
        }
        intent.putExtra(Intents.EXTRA_MAX_IMAGE_WIDTH, 802);
        intent.putExtra(Intents.EXTRA_MAX_IMAGE_HEIGHT, 390);
        intent.putExtra(Intents.EXTRA_RESULT_GRAY_SCALE, false);
        intent.putExtra(Intents.EXTRA_PREVIEW_INFO, true);
        intent.putExtra("testingd", "asg-test");
        startActivityForResult(intent, REQUEST_CAMERA);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "Request Code: " + requestCode + " ResultCode: " + resultCode);
        if (requestCode == REQUEST_CAMERA) {
            if (resultCode == RESULT_OK) {
                final ResultData resultScanner = data.getParcelableExtra(Intents.EXTRA_RESULT);
                if (resultScanner != null) {
                    Log.d(TAG, "Scanner OK: " + resultScanner.toString());
                    try {
                        backToKonyWithImage(data, resultScanner);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        Log.e(TAG, "onActivityResult:", ex);
                    }
                } else {
                    backToKony("Lo sentimos, no se pudo procesar el cheque");
                }
                String dataImg = data.getStringExtra("imgBase64");
                Log.d(TAG, "&&&&&output --- " + dataImg);
            } else if (resultCode == RESULT_CANCELED) {
                backToKony("Scaneo cancelado");
            } else {
                backToKony("Intentelo nuevamente");
            }
        } else {
            //super.onActivityResult(requestCode, resultCode, data);
        }
        Log.d(TAG, "EL FIN !!! ");
    }

    public void backToKony (String data) {
        Log.d(TAG, "&&&&&output" + data);
//        if (mCallback == null) {
//            try {
//                //Toast.makeText(this, "Scanned: " + data, Toast.LENGTH_LONG).show();
//            } catch (Exception e) {
//                e.printStackTrace();
//                //Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
//            }
//        } else {
            Log.d(TAG, "&&&&&output" + data);
            try {
                //mCallback.execute(new Object[] {"FAIL"});
                finish();
            } catch (Exception e) {
                e.printStackTrace();
                //Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
            }
//        }
    }

    public void backToKonyWithImage (Intent intent, ResultData resultData) {
//        if (mCallback != null) {
            Log.d(TAG, " resultData - " + resultData);
            try {
                //Toast.makeText(this, "Scanned: " + resultData, Toast.LENGTH_LONG).show();
                if (resultData.isSuccessful()) {
                    File file = new File(resultData.getImage());
                    Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
                    String b64 = bitmapToBase64(bmp);
                    Log.d(TAG, " base64length-- " + b64.length());
                    byte[] bytes = convertBitmapToByteArray(bmp);
                    Log.d(TAG, " byteslength-- " + bytes.length);
                    base64 = (new Object[] {b64});

                    intent.putExtra("imgBase64",b64);
                    String dataImg = intent.getStringExtra("imgBase64");
                    Log.d(TAG, " dataImg --- " + dataImg);

                    mCallback.execute(new Object[] {b64});
                } else {
                    Log.d(TAG, "&&&&&output : FAIL");
                    //mCallback.execute(new Object[] {"FAIL"});
                }
            } catch (Exception e) {
                e.printStackTrace();
                //Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
            }
            finish();
//        } else {
//            Log.d(TAG, "&&&&&output" + resultData);
//            try {
//                //Toast.makeText(this, "Scanned: " + resultData, Toast.LENGTH_LONG).show();
//                if (resultData.isSuccessful()) {
//                    File file = new File(resultData.getImage());
//                    Bitmap bmp = BitmapFactory.decodeFile(file.getAbsolutePath());
//                    String b64 = bitmapToBase64(bmp);
//                    Log.d(TAG, "&&&&&outputbase64length" + b64.length());
//                    byte[] bytes = convertBitmapToByteArray(bmp);
//                    Log.d(TAG, "&&&&&outputbyteslength" + bytes.length);
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//                Log.d(TAG, e.getMessage());
//                //Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();
//            }
//            finish();
//        }
    }

    public String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream .toByteArray();
        String encoded = Base64.encodeToString(byteArray, Base64.DEFAULT);
        return encoded;
    }

    public Bitmap base64ToBitmap(String b64) {
        byte[] imageAsBytes = Base64.decode(b64.getBytes(), Base64.DEFAULT);
        return BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
    }

    public byte[] convertBitmapToByteArray (Bitmap bitmap) {
        ByteBuffer byteBuffer = ByteBuffer.allocate(bitmap.getByteCount());
        bitmap.copyPixelsToBuffer(byteBuffer);
        byteBuffer.rewind();
        return byteBuffer.array();
    }

    public static Bitmap convertByteArrayToBitmap(byte[] src){
        return BitmapFactory.decodeByteArray(src, 0, src.length);
    }

    public String getBase64(Intent intent){

        String message= intent.getStringExtra("imgBase64");

        Log.d("&&&&&TestActivity44", message);
        Log.d("&&&&&TestActivity33", "get data");
        Log.d("&&&&&TestActivity33",base64.toString());

        return message;
    }
}
